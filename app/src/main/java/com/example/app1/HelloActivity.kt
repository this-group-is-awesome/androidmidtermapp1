package com.example.app1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class HelloActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hello)

        val name = intent.getStringExtra("EXTRA NAME")
        val textView = findViewById<TextView>(R.id.textView)
        textView.text = name

        val actionBar = supportActionBar

        actionBar!!.title = "HELLO"
    }
}