package com.example.app1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import org.w3c.dom.Text

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val helloButton = findViewById<Button>(R.id.helloButton)
        helloButton.setOnClickListener {
            val name = findViewById<TextView>(R.id.name).text.toString()

            Intent(this,HelloActivity::class.java).also {
                it.putExtra("EXTRA NAME", name)
                startActivity(it)
            }


        }

        val actionBar = supportActionBar

        actionBar!!.title = "HELLO"

    }
}